package me.jvt.examples;

import static com.github.valfirst.slf4jtest.Assertions.assertThat;
import static com.github.valfirst.slf4jtest.LoggingEvent.debug;
import static com.github.valfirst.slf4jtest.LoggingEvent.error;
import static com.github.valfirst.slf4jtest.LoggingEvent.info;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import com.github.valfirst.slf4jtest.TestLogger;
import com.github.valfirst.slf4jtest.TestLoggerFactory;
import uk.org.lidalia.slf4jext.Level;

class ClassThatLogsTest {

  private final TestLogger logger = TestLoggerFactory.getTestLogger(ClassThatLogs.class);
  private final ClassThatLogs sut = new ClassThatLogs();

  @AfterEach
  void tearDown() {
    logger.clear();
  }

  @Test
  void methodLogsErrorWhenBooleanIsTrue() {
    // given

    // when
    sut.doSomething(true);

    // then
    assertThat(logger).hasLogged(Level.ERROR, "this is because there's a boolean=true");
  }

  @Test
  void methodDoesNotLogErrorWhenBooleanIsFalse() {
    // given

    // when
    sut.doSomething(false);

    // then
    assertThat(logger).hasNotLogged(Level.ERROR, "this is because there's a boolean=true");
  }

  @Test
  void methodLogsInfoRegardless() {
    // given

    // when
    sut.doSomething(false);

    // then
    assertThat(logger).hasLogged(Level.INFO, "this is happening no matter what");
  }

  @Test
  void methodLogsFormatStringsInDebugMode() {
    // given

    // when
    sut.doSomething(false);

    // then
    assertThat(logger).hasLogged(Level.DEBUG, "The boolean passed in has value {}", false);
  }
}
